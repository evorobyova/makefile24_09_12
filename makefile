res.out:  mafia.o citizen.o main.o
		g++ -Wall -o res.out mafia.o citizen.o main.o
run: res.out
		./res.out
main.o: main.cpp mafia.h citizen.h person.h
		g++ -Wall -c main.cpp -o main.o
citizen.o: citizen.cpp person.h citizen.h
		g++ -Wall -c citizen.cpp -o citizen.o
mafia.o: mafia.cpp person.h mafia.h
		g++ -Wall -c mafia.cpp -o mafia.o

